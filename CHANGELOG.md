# Changelog

## 6.2.1 - 2025-02-25

### Fixed

- #28 Prendre en compte `#URL_DOCUMENT_FICHIER` (SPIP 4.4+), sans altérer `#URL_DOCUMENT`

## 6.2.0 - 2025-02-03

### Security

- #27 Une session anonyme avec un type de restriction faible ne doit pas ouvrir toutes les portes

### Added

- !28 ajustements UX et visuels du sélecteur de rubriques dans la conf d'une zone
- !28 filtre pour n'afficher que les rubriques sélectionnées dans l'édition d'une zone

### Changed

- Coding Style: Ajout de `rector.php` et `ecs.php` et normalisation du code
- i18n: Syntaxe simplifiée des fichiers de langue

### Fixed

- !27 Warning, Undefined array key "id_404"
- i18n: Mise à jour des traductions.

## 6.1.0 - 2024-07-05

### Fixed

- #24 Correction (warning) en PHP 8.2

## 6.0.2 - 2023-10-11

### Fixed

- #22 Correction (warning) suite à #20 avec des documents inexistants

## 6.0.1 - 2023-10-05

### Fixed

- #20 spip/medias#4931 prendre en compte l'option `htaccess` dans l'appel à `autoriser('voir','document'...)`
- #19 Éviter une fatale (PHP 8+) en l’absence de fonction `ini_set` lors de l’envoi des documents restreints

## 6.0.0 - 2023-05-22

### Changed

- Compatible SPIP 4.1 minimum et SPIP 4.2

## 5.0.1 - 2022-12-09

### Fixed

- #5 Rétablir l’accès aux logos

## 5.0.0 - 2022-07-13

### Changed

- Version compatible SPIP 4.0 & 4.1
