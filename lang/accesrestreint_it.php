<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/accesrestreint?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// A
	'ajouter_droits_auteur' => 'Aggiungimi i diritti di accesso in questa zona',
	'alerte_table_breves_documents' => '<strong>Attenzione!</strong> Il tuo sito utilizza il plugin <strong>Accesso Limitato</strong> ma il plugin <strong>Brèves</strong>non è attivo: corri il rischio di <strong>visualizzare alcuni documenti</strong> che fino a quel momento erano ad accesso limitato. 
Si consiglia di riattivarlo o, se si è sicuri di non utilizzarlo, di eliminare definitivamente la tabella spip_breves.', # MODIF
	'alerte_table_breves_forum' => '<strong>Attenzione!</strong> Il tuo sito utilizza il plugin <strong>Accesso Limitato</strong> ma il plugin <strong>Brèves</strong>non è attivo: corri il rischio di <strong>visualizzare alcuni forum</strong> che fino a quel momento erano ad accesso limitato. 
Si consiglia di riattivarlo o, se si è sicuri di non utilizzarlo, di eliminare definitivamente la tabella spip_breves.', # MODIF
	'aucune_zone' => 'Nessuna zona',
	'auteur' => '@nb@ autori',
	'auteurs' => '@nb@ autori',
	'auteurs_tous' => 'Tutte le persone collegate',

	// B
	'bouton_configurer_acces' => 'Configurazione dell’accesso .htaccess',
	'bouton_creer_la_zone' => 'Crea la nuova zona',

	// C
	'colonne_id' => 'Num',
	'confirmer_ajouter_auteurs' => 'Sei sicuro di voler aggiungere questo autore alla zona?',
	'confirmer_retirer_auteur_zone' => 'Sei sicuro di voler rimuovere questo autore dalla zona?',
	'confirmer_retirer_auteurs' => 'Sei sicuro di voler rimuovere tutti gli autori da questa zona?',
	'confirmer_retirer_rubrique_zone' => 'Sei sicuro di voler rimuovere questa rubrica dalla zona?',
	'confirmer_supprimer_zone' => 'Sei sicuro di voler eliminare questa zona?',
	'creer_zone' => 'Crea una nuova zona',

	// D
	'descriptif' => 'Descrizione',

	// E
	'explication_creer_htaccess' => 'Questa opzione vieta la lettura dei documenti allegati se il testo a cui si riferiscono non è pubblicato.',

	// I
	'icone_menu_config' => 'Accesso limitato',
	'icone_supprimer_zone' => 'Elimina questa zona',
	'info_1_zone' => '1 zona',
	'info_acces_restreint' => 'L’accesso a questa pagina è limitato; fai il login per accedervi',
	'info_ajouter_auteur' => 'Aggiungi questo autore',
	'info_ajouter_auteurs' => 'Aggiungi tutti gli autori',
	'info_ajouter_zones' => 'Aggiungi tutte le zone',
	'info_aucun_acces' => 'Nessun accesso consentito',
	'info_aucun_auteur' => 'Nessun autore nella zona',
	'info_aucune_zone' => 'Nessuna zona',
	'info_auteurs_lies_zone' => 'Gli autori che possono accedere a questa zona',
	'info_lien_action_proteger' => 'Proteggi l’accesso a questa rubrica.',
	'info_nb_zones' => '@nb@ zone',
	'info_page' => 'Questa pagina permette di gestire le zone di accesso limitato del proprio sito ',
	'info_retirer_auteurs' => 'Rimuovi tutti gli autori',
	'info_retirer_zone' => 'Togli la zona',
	'info_retirer_zones' => 'Togli tutte le zone',
	'info_rubrique_dans_zone' => 'Questa rubrica appartiene alla zona:',
	'info_rubrique_dans_zones' => 'Questa rubrica appartiene alle zone',
	'info_vous_avez_acces_a_cette_zone' => 'Hai accesso a questa zona',
	'info_vous_navez_pas_acces_a_cette_zone' => 'Non hai accesso a questa zona',

	// L
	'label_creer_htaccess' => 'Accesso ai documenti allegati tramite il loro URL',
	'label_creer_htaccess_non' => 'autorizzare la lettura',
	'label_creer_htaccess_oui' => 'vietare la lettura',
	'label_droits_acces' => 'Diritti di accesso',
	'label_filtrer_rubriques' => 'Filtro',
	'label_titre_zone' => 'Titolo',

	// M
	'modifier_zone' => 'Modifica zona',

	// P
	'page_zones_acces' => 'Accesso Limitato',
	'par_titre' => 'Per titolo',
	'placeholder_filtrer_rubriques' => 'Titolo di una rubrica',
	'privee' => 'Privata',
	'publique' => 'Pubblica',

	// R
	'rubrique' => '@nb@ rubriche',
	'rubriques' => '@nb@ rubriche',
	'rubriques_zones_acces' => 'Rubriche della zona',

	// S
	'selectionner_une_zone' => 'Seleziona una zona',

	// T
	'texte_ajouter_zone' => 'Aggiungi una zona',
	'texte_creer_associer_zone' => 'Crea ed associa una zona',
	'titre' => 'Titolo',
	'titre_ajouter_zone' => 'Unisci la zona',
	'titre_boite_protegee_non' => 'Accesso non protetto',
	'titre_boite_protegee_oui' => 'Accesso protetto',
	'titre_cadre_modifier_zone' => 'Modifica una zona',
	'titre_page_config' => 'Configurazione dell’accesso',
	'titre_table' => 'Tutte le zone di accesso',
	'titre_zone_acces' => 'Zona ad accesso limitato',
	'titre_zones_acces' => 'Zone ad accesso limitato',
	'toutes' => 'Tutte',

	// V
	'voir_toutes' => 'Vedi tutte le zone',

	// Z
	'zone_numero' => 'ZONA NUMERO:',
	'zone_restreinte_autoriser_si_connexion_label' => 'Consenti l’accesso a questa zona a chiunque sia connesso',
	'zone_restreinte_espace_prive' => 'Limitare l’accesso a questa zona nell’area riservata',
	'zone_restreinte_publique' => 'Limitare l’accesso a questa zona nell’area pubblica',
];
