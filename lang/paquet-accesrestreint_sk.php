<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-accesrestreint?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// A
	'accesrestreint_description' => '_ V každej zóne sú rubriky.

_ Autori môžu byť so zónami prepojení cez prístupoé práva.

_ Zavedené cyly SPIPu sa upravia  podľa zistených prístupových práv návštevníka.',
	'accesrestreint_nom' => 'Obmedzený prístup',
	'accesrestreint_slogan' => 'Riadenie zón s obmedzeným prístupom',
];
