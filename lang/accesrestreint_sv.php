<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/accesrestreint?lang_cible=sv
// ** ne pas modifier le fichier **

return [

	// P
	'privee' => 'Privat',
	'publique' => 'Publik',
];
