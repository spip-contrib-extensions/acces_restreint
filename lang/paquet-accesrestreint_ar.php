<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-accesrestreint?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// A
	'accesrestreint_description' => '_ كل مجال يحتوي على أقسام.

_ يمكن للمؤلفين الاشتراك في مجالات ليحصلوا على إذن الدخول اليها.

_ يتم تحوير كل حلقات SPIP القياسية لترشيح نتائجها حسب حقوق الزائر.',
	'accesrestreint_nom' => 'الدخول المحصور',
	'accesrestreint_slogan' => 'إدارة مجالات الدخول المحصور',
];
