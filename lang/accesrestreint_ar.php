<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/accesrestreint?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// A
	'ajouter_droits_auteur' => 'إعطائي حق الدخول الى هذه المنطقة',
	'alerte_table_breves_documents' => '<strong>تحذير!</strong> موقعكم يستخدم ملحق <strong>دخول محصور</strong> لكن الملحق <strong>الأخبار</strong> غير نشط: 
		قد يؤدي ذلك الى <strong>عرض مستندات</strong> كانت ضمن الدخول المحصور. 
		يستحسن إعادة تثبيت ملحق الأخبار او اذا كنتم متأكدين من عدم استخدامه، حذف جدول spip_breves.', # MODIF
	'alerte_table_breves_forum' => '<strong>تحذير!</strong> موقعكم يستخدم ملحق <strong>دخول محصور</strong> لكن الملحق <strong>الأخبار</strong> غير نشط: 
		قد يؤدي ذلك الى <strong>عرض منتديات</strong> كانت ضمن الدخول المحصور. 
		يستحسن إعادة تثبيت ملحق الأخبار او اذا كنتم متأكدين من عدم استخدامه، حذف جدول spip_breves.', # MODIF
	'aucune_zone' => 'لا يوجد أي مجال',
	'auteur' => '@nb@ مؤلف',
	'auteurs' => '@nb@ مؤلف',
	'auteurs_tous' => 'جميع الأشخاص المتصلين',

	// B
	'bouton_configurer_acces' => 'إعداد أذونات الدخول في htaccess.',
	'bouton_creer_la_zone' => 'إنشاء المجال الجديد',

	// C
	'colonne_id' => 'الرقم',
	'confirmer_ajouter_auteurs' => 'هل انت متأكد من إضافة هذا المؤلف الى المجال؟',
	'confirmer_retirer_auteur_zone' => 'هل تريد فعلاً سحب هذا المؤلف من المجال؟',
	'confirmer_retirer_auteurs' => 'هل تريد فعلاً سحب جميع المؤلفين من المجال؟',
	'confirmer_retirer_rubrique_zone' => 'تأكيد سحب هذا القسم من هذا المجال؟',
	'confirmer_supprimer_zone' => 'هل تريد فعلاً حذف هذا المجال؟',
	'creer_zone' => 'إنشاء مجال جديد',

	// D
	'descriptif' => 'الوصف',

	// E
	'explication_creer_htaccess' => 'هذا الخيار يمنع الاطلاع على المستندات المرفقة اذا كان النص المرتبطة به غير منشور',

	// I
	'icone_menu_config' => 'دخول محصور',
	'icone_supprimer_zone' => 'حذف هذ المجال',
	'info_1_zone' => 'مجال واحد',
	'info_acces_restreint' => 'الدخول الى هذه الصفحة محصور. عليك التعريف بنفسك للدخول اليها',
	'info_ajouter_auteur' => 'إضافة مؤلف',
	'info_ajouter_auteurs' => 'إضافة جميع المؤلفين',
	'info_ajouter_zones' => 'إضافة كل المجالات',
	'info_aucun_acces' => 'لا يوجد أي دخول مسموح',
	'info_aucun_auteur' => 'لا يوجد أي مؤلف في المجال',
	'info_aucune_zone' => 'لا يوجد أي مجال',
	'info_auteurs_lies_zone' => 'المؤلفون المسموح لهم الدخول الى هذا المجال',
	'info_lien_action_proteger' => 'حماية الوصول الى هذا القسم.',
	'info_nb_zones' => '@nb@ مجال',
	'info_page' => 'تتيح لك هذه الصفحة إدارة مجالات الدخول المحصور في الموقع',
	'info_retirer_auteurs' => 'سحب جميع المؤلفين',
	'info_retirer_zone' => 'سحب من المنطقة',
	'info_retirer_zones' => 'إلغاء كل المجالات',
	'info_rubrique_dans_zone' => 'هذا القسم هو ضمن المجال:',
	'info_rubrique_dans_zones' => 'هذا القسم هو ضمن المجالات:',
	'info_vous_avez_acces_a_cette_zone' => 'انت مخول(ة) الوصول الى هذا المجال',
	'info_vous_navez_pas_acces_a_cette_zone' => 'انت غير مخول(ة) الوصول الى هذا المجال',

	// L
	'label_creer_htaccess' => 'الوصول الى المستندات المرفقة من خلال عناوين URL',
	'label_creer_htaccess_non' => 'السماح بالاطلاع',
	'label_creer_htaccess_oui' => 'منع الاطلاع',
	'label_droits_acces' => 'حقوق الوصول',
	'label_filtrer_rubriques' => 'ترشيح',
	'label_titre_zone' => 'الاسم',

	// M
	'modifier_zone' => 'تعديل المجال',

	// P
	'page_zones_acces' => 'دخول محصور',
	'par_titre' => 'حسب الاسم',
	'placeholder_filtrer_rubriques' => 'عنوان قسم',
	'privee' => 'الخاص',
	'publique' => 'العمومي',

	// R
	'rubrique' => '@nb@ قسم',
	'rubriques' => '@nb@ قسم',
	'rubriques_zones_acces' => 'أقسام المجال',

	// S
	'selectionner_une_zone' => 'تحديد منطقة',

	// T
	'texte_ajouter_zone' => 'إضافة مجال',
	'texte_creer_associer_zone' => 'إنشاء مجال جديد والاشتراك فيها',
	'titre' => 'الاسم',
	'titre_ajouter_zone' => 'الربط بالمجال',
	'titre_boite_protegee_non' => 'وصول غير محمي',
	'titre_boite_protegee_oui' => 'وصول محمي',
	'titre_cadre_modifier_zone' => 'تعديل مجال',
	'titre_page_config' => 'إعداد الدخول',
	'titre_table' => 'كل مجالات الدخول',
	'titre_zone_acces' => 'مجال وصول محصور',
	'titre_zones_acces' => 'مجالات الدخول المحصور',
	'toutes' => 'الكل',

	// V
	'voir_toutes' => 'عرض كل المجالات',

	// Z
	'zone_numero' => '<strong>مجال رقم</strong>',
	'zone_restreinte_autoriser_si_connexion_label' => 'السماح بالدخول الى هذا المجال لأي شخص متصل',
	'zone_restreinte_espace_prive' => 'حصر الدخول الى هذا المجال في المجال الخاص',
	'zone_restreinte_publique' => 'حصر الدخول الى هذاالمجال في الموقع العمومي',
];
