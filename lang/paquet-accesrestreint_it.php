<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-accesrestreint?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// A
	'accesrestreint_description' => '-* Ogni zona contiene rubriche.
-* Gli autori possono essere associati alle zone per avere il diritto di accedervi.
-* Tutti i cicli nativi di SPIP vengono sovraccaricati per filtrare i risultati in base ai diritti del visitatore.',
	'accesrestreint_nom' => 'Accesso Limitato',
	'accesrestreint_slogan' => 'Gestione delle zone ad accesso limitato',
];
