<?php

/**
 * Plugin Acces Restreint 5.0 pour Spip 4.x
 * Licence GPL (c) depuis 2006 Cedric Morin
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Generer l'url d'un document dans l'espace public, par défaut l'url de son fichier
 *
 * @param int $id
 * @param string $args
 * @param string $ancre
 * @param string $public
 * @param string $connect
 * @return string
 *
 */
function urls_generer_url_document_dist($id, $args = '', $ancre = '', $public = null, $connect = '') {
	include_spip('inc/utils');
	$generer_url = charger_fonction('generer_url_document_fichier', 'urls');
	
	return $generer_url($id, $args, $ancre, $public, $connect);
}
