<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use SpipLeague\Component\Rector\Set\SpipLevelSetList;
use SpipLeague\Component\Rector\Set\SpipSetList;

return RectorConfig::configure()
	->withPaths([
		__DIR__ . '/action',
		__DIR__ . '/base',
		__DIR__ . '/formulaires',
		__DIR__ . '/inc',
		__DIR__ . '/lang',
		__DIR__ . '/public',
		__DIR__ . '/urls',
	])
	// uncomment to reach your current PHP version
	// ->withPhpSets()
	->withTypeCoverageLevel(0)
	->withSets([SpipSetList::SPIP_41, SpipLevelSetList::UP_TO_SPIP_41]);
